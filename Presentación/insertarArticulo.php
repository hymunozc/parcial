<?php
$creado = false;
if(isset($_POST["crear"])){
    $articulo = new Articulo("", $_POST["Titulo"], $_POST["Descripcion"], , $_POST["Fecha"]);
    $articulo -> crear();
    $creado = true;
}
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-3"></div>
		<div class="col-6">
			<div class="card">
				<div class="card-header">
					<h3>Crear Articulo</h3>
				</div>
				<div class="card-body">
					<?php if ($creado) { ?>						
						<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							<strong>Datos ingresados</strong>
							<button type="button" class="close" data-dismiss="alert"
								aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
					<?php } ?>
					<form
						action=<?php echo "index.php?pid=" . base64_encode("Presentación/insertarArticulo.php") ?>
						method="post">
						<div class="form-group">
							<input type="text" name="Titulo" class="form-control"
								placeholder="Titulo" required="required">
						</div>
						<div class="form-group">
							<input type="text" name="Descripcion" class="form-control"
								placeholder="Descripcion" required="required">
                        </div>
                        <div class="form-group">
							<input type="text" name="Fecha" class="form-control"
								placeholder="Fecha" required="required">
						</div>
						<div class="form-group">
							<button type="submit" name="Insertar" class="btn btn-primary">Crear</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>