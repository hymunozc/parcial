<?php
require "persistencia/ArticuloDAO.php";

class Articulo{
    private $idArticulo;
    private $titulo;
    private $descripcion;
    private $fecha;;
    
    public function getIdArticulo()
    {
        return $this->idArticulo;
    }

    public function gettitulo()
    {
        return $this->titulo;
    }
    public function getdescripcion()
    {
        return $this->descripcion;
    }

    public function getfecha()
    {
        return $this->fecha;
    }

    function Articulo ($pidArticulo="", $ptitulo="", $pdescripcion="", $pfecha="") {
        $this -> idArticulo = $pidArticulo;
        $this -> titulo = $ptitulo;
        $this -> descripcion = $pdescripcion;
        $this -> fecha = $pfecha;
        $this -> conexion = new Conexion();
        $this -> autorDAO = new ArticuloDAO ($pidArticulo, $ptitulo, $pdescripcion, $pfecha);        
    }
    
    function consultar(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> consultar());
        $this -> conexion -> cerrar();        
        $resultado = $this -> conexion -> extraer();
        $this -> titulo = $resultado[0];
        $this -> descripcion = $resultado[1];
        $this -> fecha = $resultado[2];
    }

    function crear(){
        $this -> conexion -> abrir();        
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> crear());
        $this -> conexion -> cerrar();
    }
    
    function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> ArticuloDAO -> consultarTodos());
        $this -> conexion -> cerrar();
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($rticulos, new Autor($resultado[0], $resultado[1], $resultado[2], $resultado[3]));
        }
        return $articulos;
    }